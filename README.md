Installation:
1. $ git clone git@gitlab.com:igoryspringer/symfonycrud.git
2. $ composer install --ignore-platform-reqs
3. $ bin/console doctrine:database:create
4. $ bin/console doctrine:schema:update --force
5. $ bin/console doctrine:fixtures:load
6. $ bin/console server:start

Database [PostgreSQL]:
1. $ sudo -u postgres psql postgres
2. $ \password postgres
3. Enter double password for user postgres

Сompleted tasks:
1. CRUD
2. Twig
3. Gitlab-CI
4. Testing
5. DomCrawler
6. CssSelector
7. Console Commands & Console Commands Test
8. ParseSymfonyCommand
9. Doctrine & Doctrine Associations / Relations
10. Symfony Guard & SecurityControllerTest
11. User Fixtures
12. MakerBundle
13. Locale
14. HTTP Client
15. Packagist
16. Symfony Mailer
17. Генерации PDF, XLS, QR-code